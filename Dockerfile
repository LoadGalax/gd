FROM node

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package*.json ./
COPY src ./

RUN npm install

COPY . .

EXPOSE 1115

CMD [ "node", "src/index.js"  ]