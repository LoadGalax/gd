const { Router } = require("express");
const app = require("express");
const path = require('path');
const router = Router();
const Image = require('../models/Image');
const Game = require('../models/Game');
const { resourceLimits } = require("worker_threads");
const idgame = require('../index')
const upload = require('../helpers/upload');
const fs = require('fs');
// Routes

/* Web Navigation */

/* Redirect To index */
router.get('/', (req, res) => {
    res.redirect('index?page=1');
});

/* Main Page */
const page = 1;
router.get('/index', async(req, res) => {
    //NormalPage
     const page = parseInt(req.query.page);
     /*     const limit = parseInt(req.query.limit); */
    /* const page = 2 */
    const pageMaxLimit = 4;
    const pageMinLimit = 1;
    const limit = 12;
    const skipIndex = (page - 1) * limit;

    const GameId = await Game.find()
    .sort({_id: 1})
    .limit(limit)
    .skip(skipIndex)
    .lean();
    const Id = await Game.find().lean();
    const cover = GameId.cover
    res.render('index', { Id, cover, GameId, page, pageMaxLimit, pageMinLimit, });
    /* res.render('index') */

});


router.get('/New-Game-form', async(req, res) => {
    res.render('New-Game')
})
router.get('/regenerate', async(req, res) => {
    
    await Game.deleteMany({ title: { $exists: true} }) 
    const games = './src/public/game';
    fs.readdir(games, { withFileTypes: true }, async(err, files) => {
        files
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)
        .forEach(async file => {
            const nm = Game.findOne({root:file})
            const met = file
            if (fs.existsSync('./src/public/game/' + file + '.json')){
            }else{
                console.log('NO Encontrado')
                  fs.writeFileSync('./src/public/game/' + file + '.json', '{"so":" ","ram":" ","storage":" ","size":" ","file":" ","text1":" ","text2":" ","text3":" ","jap":false,"__v":0} ');
            }
            if (await nm == null) {          
                const meta = require('../public/game/' + file + '.json')
                const GameId = new Game();
                GameId.root = file
                GameId.title = file;
                GameId.text1 = meta.text1;
                GameId.text2 = meta.text2;
                GameId.text3 = meta.text3;
                GameId.so = meta.so;
                GameId.ram = meta.ram;
                GameId.storage = meta.storage;
                GameId.size = meta.size;
                GameId.jap = meta.jap ? true : false;
                GameId.save();
            } else{
            }        
        });
      });
      //wait 1 s
      await sleep(1000)
      function sleep(ms) {
      return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
} 
 res.redirect('index')
})

router.post('/New-Game-form', async(req, res) => {
    const GameId = new Game();
    GameId.title = req.body.title;
    GameId.root = req.body.root;
    GameId.text1 = req.body.text1;
    GameId.text2 = req.body.text2;
    GameId.text3 = req.body.text3;
    GameId.so = req.body.so;
    GameId.ram = req.body.ram;
    GameId.storage = req.body.storage;
    GameId.size = req.body.size;
    GameId.jap = req.body.jap ? true : false;
    /*   GameId.jap = req.body.jap; */
    /* console.log(GameId); */
    await GameId.save();
    /*  console.log(GameId); */
    /* res.render('New-Game') */
    res.render('New-Game');
})

router.get('/show-game/:id', async(req, res) => {
    const { id } = req.params;
    const GameId = await Game.findById(id);
    const root = GameId.root
    const title = GameId.title
    const text1 = path.join(GameId.text1)
    const text2 = path.join(GameId.text2)
    const text3 = path.join(GameId.text3)
    const so = GameId.so
    const ram = GameId.ram
    const storage = GameId.storage
    const size = GameId.size
    const tt = await Game.findOne({root:'Forager'})
    console.log(tt.root)

    res.render('Show-Games', { root, title, id, text1, text2, text3, so, ram, storage, size }, );
})

router.get('/delete/:id', async(req, res) => {
    const { id } = req.params;
    const GameId = await Game.findByIdAndDelete(id);
    res.redirect('/')
})
router.get('/edit/:id', async(req, res) => {
    const { id } = req.params;
    const GameId = await Game.findById(id);
    const root = GameId.root
    const title = GameId.title
    const text1 = path.join(GameId.text1)
    const text2 = path.join(GameId.text2)
    const text3 = path.join(GameId.text3)
    const so = GameId.so
    const ram = GameId.ram
    const storage = GameId.storage
    const size = GameId.size
    const tt = await Game.findOne({root:'Forager'})
    console.log(tt.root)

    res.render('Edit', { root, title, id, text1, text2, text3, so, ram, storage, size }, );
 
})

router.post('/update/:id', async(req, res) => {
    const { id } = req.params;
    const GameId = await Game.findByIdAndUpdate(id);
    GameId.title = req.body.title;
    GameId.root = req.body.root;
    GameId.text1 = req.body.text1;
    GameId.text2 = req.body.text2;
    GameId.text3 = req.body.text3;
    GameId.so = req.body.so;
    GameId.ram = req.body.ram;
    GameId.storage = req.body.storage;
    GameId.size = req.body.size;
    GameId.jap = req.body.jap ? true : false;
    /*   GameId.jap = req.body.jap; */
    /* console.log(GameId); */
    await GameId.save();
    const jsonContent = {"so":req.body.so,"ram":req.body.ram,"storage":req.body.storage,"size":req.body.size,"file":" ","text1":req.body.text1,"text2":req.body.text2,"text3":req.body.text3,"jap":req.body.jap ? true : false,"__v":0}
    const jsontString = JSON.stringify(jsonContent)
    console.log(jsontString)
    fs.writeFileSync('./src/public/game/' + req.body.root + '.json', jsontString);
    res.redirect('/');
})

router.post('/uploads-image', async(req, res) => {
    const image = new Image();
})


module.exports = router;



