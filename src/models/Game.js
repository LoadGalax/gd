const { Schema, model } = require("mongoose");

const gameSchema = new Schema({
    title: { type: String, default: 'null'},
    root: { type: String, default: 'null' },
    url: { type: String, default: 'null' },
    so: { type: String, default: 'null' },
    ram: { type: String, default: 'null' },
    storage: { type: String, default: 'null' },
    size: { type: String, default: 'null' },
    file: { type: String, default: 'null' },
    text1: { type: String, default: ' ' },
    text2: { type: String, default: ' ' },
    text3: { type: String, default: ' ' },
    jap: { type: Boolean, default: false }

});



module.exports = model('Game', gameSchema);