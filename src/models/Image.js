const { Schema, model } = require("mongoose");

const imageSchema = new Schema({
    title: { type: String },
    filename: { type: String },
    path: { type: String },
    originalname: { type: String },
    mimetype: { type: String },
    size: { type: Number },
    create_at: { type: Date, default: Date.now() },
    description: { type: String },
});



module.exports = model('Image', imageSchema);