const express = require('express');
const res = require('express/lib/response');
const path = require('path');
const multer = require('multer');
const req = require('express/lib/request');
const { randomUUID } = require('crypto');
const exphbs = require('express-handlebars');
const morgan = require('morgan')
const fs = require('fs');

//Init
const app = express();
require('./database');
//Settings
app.set('port', process.env.PORT || 1115);
//hbs
const hbs = exphbs.create({
    defaultLayout: 'main',
    layoutsDir: path.join(__dirname, 'views/layauts'),
    partialsDir: path.join(__dirname, 'views/partials'),
    extname: '.hbs',
    helpers: {
        paginationP: function(value, max, min){
            if (value == max){
                return value;
            }else{
                return value+1 ;
            }
        },
        paginationR: function(value, max, min){
            if (value == min){
                return value;
            }else{
                return value-1 ;
            }
        }
    }
})
app.engine('.hbs', hbs.engine);
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));
//Setings-Multer
const storage = multer.diskStorage({
    destination: path.join(__dirname, 'public/uploads'),
    filename: (req, file, cb) => {
        cb(null, randomUUID() + path.extname(file.originalname).toLocaleLowerCase());
    }
});

//midleware
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: false }))
app.use(multer({

    storage,
    dest: path.join(__dirname, 'public/uploads'),
    limits: { fileSize: 1000000000000 },
    fileFilter: (req, file, cb) => {
        const fields = [{ name: 'image' }, { name: 'image2' }];
        const filetypes = /jpeg|jpg|png|gif/;
        const mimetype = filetypes.test(file.mimetype);
        const extname = filetypes.test(path.extname(file.originalname));
        if (mimetype && extname) {
            return cb(null, true)
        }
        cb("Error: File Error")
    }

}).single('image'));


// Global Variables
/* const idgame = 'gaemess';
module.exports = ('IdGame', idgame); */
// Routes
app.use(require('./routes/index.routes'));
// Static files
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '../game')));
/* console.log(path.join(__dirname, './public')) */
//

//Folder-Scan-To-Db
const games = './src/public/game';
const Game = require('./models/Game');
const helpers = require('handlebars-helpers');

fs.readdir(games, { withFileTypes: true }, async(err, files) => {
    files
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)
    .forEach(async file => {
        const nm = Game.findOne({root:file})
        if (fs.existsSync('./src/public/game/' + file + '.json')){
        }else{
            
            console.log('NO Encontrado')
              fs.writeFileSync('./src/public/game/' + file + '.json', '{"so":" ","ram":" ","storage":" ","size":" ","file":" ","text1":" ","text2":" ","text3":" ","jap":false,"__v":0} ');
        }
        
        if (await nm == null) {          
            const meta = require('../src/public/game/' + file + '.json')
            const GameId = new Game();
            GameId.root = file
            GameId.title = file;
            GameId.text1 = meta.text1;
            GameId.text2 = meta.text2;
            GameId.text3 = meta.text3;
            GameId.so = meta.so;
            GameId.ram = meta.ram;
            GameId.storage = meta.storage;
            GameId.size = meta.size;
            GameId.jap = meta.jap ? true : false;
            GameId.save();
        } else{
        }        
    });
  });


//

process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled Rejection at:', reason.stack || reason)
    console.log(promise)
  })
//Start server
app.listen(app.get('port'), () => {

    console.log(`Server on port`, app.get('port'))
})